﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationType.Permissions
{
    public static class Permissions
    {
        public const string View = "view";
        public const string Create = "create";
        public const string Edit = "edit";
        public const string Delete = "delete";
    }
    public static class TokenClaimType
    {
        public const string Permission = "permission";
    }
}

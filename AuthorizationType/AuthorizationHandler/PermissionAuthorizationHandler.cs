﻿using AuthorizationType.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationType.AuthorizationHandler
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        //UserManager<IdentityUser> _userManager;
        //RoleManager<IdentityRole> _roleManager;

        //public PermissionAuthorizationHandler(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        //{
        //    _userManager = userManager;
        //    _roleManager = roleManager;
        //}

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if(context.User == null)
            {
                return;
            }

            var user = context.User;
            var claims = user.Claims;
            var permissionClaimValid = claims.Where(s => s.Type == TokenClaimType.Permission && s.Value == requirement.Permission && s.Issuer == "SAC_Issuer");
            if (permissionClaimValid.Any())
            {
                context.Succeed(requirement);
                return;
            }
            else
            {
                context.Fail();
            }
        }
    }
}

﻿using AuthorizationType.Permissions;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationType.Helper
{
    public class JwtService
    {
        public string Login(string username, string password)
        {
            string token = "Fail";

            var tokenHandler = new JwtSecurityTokenHandler();
            var claimCollection = new List<Claim>();
            claimCollection.Add(new Claim(ClaimTypes.NameIdentifier, username));
            claimCollection.Add(new Claim(TokenClaimType.Permission, Permissions.Permissions.View));
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claimCollection);
            var key = Encoding.ASCII.GetBytes("SAC_KEY_sadasdasasdasdasda");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimsIdentity,
                Issuer = "SAC_Issuer",
                Audience = "SAC_Audience",
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var newToken = tokenHandler.CreateToken(tokenDescriptor);
            if(newToken != null)
            {
                token = tokenHandler.WriteToken(newToken);
                return token;
            }
            return token;
        }
    }
}
